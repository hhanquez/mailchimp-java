

This is a Java wrapper around the MailChimp API.

Fork from https://github.com/micheal-swiggs/mailchimp-java

#### Prerequisites

##### Lombok in IntelliJ IDEA

Source : https://www.baeldung.com/lombok-ide

###### Enabling Annotation Processing
Lombok uses annotation processing through APT, so, when the compiler calls it, the library generates new source files based on annotations in the originals.

Annotation processing isn’t enabled by default, though.

So, the first thing for us to do is to enable annotation processing in our project.

We need to go to the _Preferences | Build, Execution, Deployment | Compiler | Annotation Processors_ and make sure of the following:
* Enable annotation processing box is checked
* Obtain processors from project classpath option is selected

###### Installing the IDE Plugin
There is a dedicated plugin which makes IntelliJ aware of the source code to be generated. **After installing it, the errors go away and regular features like Find Usages, Navigate To start working.**

We need to go to the Preferences | Plugins, open the Marketplace tab, type lombok and choose _Lombok Plugin by Michail Plushnikov_:


#### Example code for using the client

    MailChimpClient mailChimpClient = MailChimpClient.builder()
      .withApiBase("us1")
      .withBasicAuthentication("abc")
      .build();
    List subscriberList = mailChimpClient.getList("123");


