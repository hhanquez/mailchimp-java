package com.mailchimp.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListMergeFieldsQuery {

    @Getter
    private Integer offset;

    @Getter
    private Integer count;

    @Getter
    private String type;

    public static ListMergeFieldsQuery firstPage() { return new ListMergeFieldsQuery(); }
}
