package com.mailchimp.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author hhanquez
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CampaignFoldersQuery {
    @Getter
    private Integer count;

    @Getter
    private Integer offset;
}
