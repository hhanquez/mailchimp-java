package com.mailchimp.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author hhanquez
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CampaignsQuery {
    @Getter
    private Integer count;

    @Getter
    private Integer offset;

    @Getter
    private String type;

    @Getter
    private String status;

    @Getter
    private String list_id;

    @Getter
    private String folder_id;

    @Getter
    private String member_id;

    @Getter
    private String before_send_time;

    @Getter
    private String since_send_time;

    @Getter
    private String before_create_time;

    @Getter
    private String since_create_time;
}
