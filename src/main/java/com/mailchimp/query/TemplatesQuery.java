package com.mailchimp.query;

import lombok.*;

/**
 * @author hhanquez
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TemplatesQuery {
    @Getter
    private Integer count;

    @Getter
    private Integer offset;

    @Getter
    private String created_by;

    @Getter
    private String type;

    @Getter
    private String category;

    @Getter
    private String folder_id;
}
