package com.mailchimp.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmailActivityQuery {


    @Getter
    private Integer offset;

    @Getter
    private Integer count;

    @Getter
    private String since;

    public static EmailActivityQuery all() {
        return new EmailActivityQuery();
    }
}