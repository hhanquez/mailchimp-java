package com.mailchimp.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListsQuery {

    @Getter
    private Integer offset;

    @Getter
    private Integer count;

    @Getter
    private String before_date_created;

    @Getter
    private String since_date_created;

    @Getter
    private String since_campaign_last_sent;

    @Getter
    private String email;

    @Getter
    private String sort_field;

    @Getter
    private String sort_dir;

    public static ListsQuery firstPage(){
        return new ListsQuery();
    }
}
