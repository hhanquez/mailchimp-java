package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author hhanquez
 */
@Data
public class EmailActivity {

    public static class Activity {
        @JsonProperty
        @Getter
        @Setter
        String action;

        @JsonProperty
        @Getter
        @Setter
        String type;

        @JsonProperty
        @Getter
        @Setter
        String timestamp;

        @JsonProperty
        @Getter
        @Setter
        String url;

        @JsonProperty
        @Getter
        @Setter
        String ip;
    }

    @JsonProperty(value = "campaign_id")
    @Getter
    @Setter
    String campaignId;

    @JsonProperty(value = "list_id")
    @Getter
    @Setter
    String listId;

    @JsonProperty(value = "list_is_active")
    @Getter
    @Setter
    Boolean listIsActive;

    @JsonProperty(value = "email_id")
    @Getter
    @Setter
    String emailId;

    @JsonProperty(value = "email_address")
    @Getter
    @Setter
    String emailAddress;

    @JsonProperty(value = "activity")
    @Getter
    @Setter
    List<Activity> activityList;
}
