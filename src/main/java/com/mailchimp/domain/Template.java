package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


/**
 * @author hhanquez
 */
@JsonIgnoreProperties(ignoreUnknown = true)//TODO: remove this when all properties are add
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Template {

    @JsonProperty
    @Getter
    @Setter
    private String id;

    @JsonProperty
    @Getter
    @Setter
    private String type;

    @JsonProperty
    @Getter
    @Setter
    private String name;

    @JsonProperty("drag_and_drop")
    @Getter
    @Setter
    private Boolean dragAndDrop;

    @JsonProperty
    @Getter
    @Setter
    private Boolean responsive;

    @JsonProperty
    @Getter
    @Setter
    private String category;

    @JsonProperty("date_created")
    @Getter
    @Setter
    private String dateCreated;

    @JsonProperty("date_edited")
    @Getter
    @Setter
    private String dateEdited;

    @JsonProperty("created_by")
    @Getter
    @Setter
    private String createdBy;

    @JsonProperty("edited_by")
    @Getter
    @Setter
    private String editedBy;

    @JsonProperty
    @Getter
    @Setter
    private Boolean active;

    @JsonProperty("folder_id")
    @Getter
    @Setter
    private String folderId;

    @JsonProperty
    @Getter
    @Setter
    private String thumbnail;

    @JsonProperty("shake_url")
    @Getter
    @Setter
    private String shakeUrl;

}
