package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author hhanquez
 */
@Data
public class Campaigns {
    @JsonProperty
    @Getter
    @Setter
    private List<Campaign> campaigns;

    @JsonProperty("total_items")
    @Getter
    @Setter
    private Integer totalItems;
}
