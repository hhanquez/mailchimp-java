package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * @author hhanquez
 */
@JsonIgnoreProperties(ignoreUnknown = true)//TODO: remove this when all properties are add
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CampaignFolder {
    @JsonProperty
    @Getter
    @Setter
    private String name;

    @JsonProperty
    @Getter
    @Setter
    private String id;

    @JsonProperty
    @Getter
    @Setter
    private Integer count;
}
