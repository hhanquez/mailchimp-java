package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author hhanquez
 */
@Data
public class CampaignFolders {
    @JsonProperty("folders")
    @Getter
    @Setter
    private List<CampaignFolder> folders;

    @JsonProperty("total_items")
    @Getter
    @Setter
    private Integer totalItems;
}
