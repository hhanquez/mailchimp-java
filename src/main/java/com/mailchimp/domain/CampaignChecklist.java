package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

/**
 * @author hhanquez
 */
@JsonIgnoreProperties(ignoreUnknown = true)//TODO: remove this when all properties are add
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CampaignChecklist {

    @JsonProperty("is_ready")
    @Getter
    @Setter
    private Boolean isReady;

    public static class Items {
        @JsonProperty
        @Getter
        @Setter
        private String type;

        @JsonProperty
        @Getter
        @Setter
        private Integer id;

        @JsonProperty
        @Getter
        @Setter
        private String heading;

        @JsonProperty
        @Getter
        @Setter
        private String details;
    }

    @JsonProperty
    @Getter
    @Setter
    private List<Items> items;
}
