package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author hhanquez
 */
@Data
public class BatchSubscribe {

    @JsonProperty
    private List<Member> members;

    @JsonProperty("update_existing")
    private Boolean updateExisting;

}
