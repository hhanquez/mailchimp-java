package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * @author hhanquez
 */
public enum CampaignType {
    Regular("regular"),
    Plaintext("plaintext"),
    Absplit("absplit"),
    Rss("rss"),
    Variate("variate");

    private final String value;

    CampaignType(final String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return this.value;
    }
}
