package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author hhanquez
 */
@Data
public class EmailActivities {
    @JsonProperty
    @Getter
    @Setter
    private List<EmailActivity> emails;

    @JsonProperty("campaign_id")
    @Getter
    @Setter
    private String campaignId;

    @JsonProperty("total_items")
    @Getter
    @Setter
    private Integer totalItems;
}
