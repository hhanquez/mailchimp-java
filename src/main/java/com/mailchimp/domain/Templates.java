package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author hhanquez
 */
@Data
public class Templates {

    @JsonProperty("templates")
    @Getter
    @Setter
    private List<Template> templates;

    @JsonProperty("total_items")
    @Getter
    @Setter
    private Integer totalItems;
}
