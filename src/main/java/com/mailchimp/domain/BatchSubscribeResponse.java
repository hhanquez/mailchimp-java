package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

/**
 * @author hhanquez
 */
@JsonIgnoreProperties(ignoreUnknown = true)//TODO: remove this when all properties are add
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BatchSubscribeResponse {

    public static class Errors {
        @JsonProperty("email_address")
        @Getter
        @Setter
        private String emailAddress;

        @JsonProperty("error")
        @Getter
        @Setter
        private String error;

        @JsonProperty("error_code")
        @Getter
        @Setter
        private String errorCode;
    }

    @JsonProperty("new_members")
    @Getter
    @Setter
    private List<Member> newMembers;

    @JsonProperty("updated_members")
    @Getter
    @Setter
    private List<Member> updatedMembers;

    @JsonProperty("errors")
    @Getter
    @Setter
    private List<Errors> errors;

    @JsonProperty("total_created")
    @Getter
    @Setter
    private Integer totalCreated;

    @JsonProperty("total_updated")
    @Getter
    @Setter
    private Integer totalUpdated;

    @JsonProperty("error_count")
    @Getter
    @Setter
    private Integer errorCount;
}
