package com.mailchimp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * @author hhanquez
 */
@JsonIgnoreProperties(ignoreUnknown = true)//TODO: remove this when all properties are add
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Campaign {

    public static class SegmentOpts {
        @JsonProperty("saved_segment_id")
        @Getter
        @Setter
        private Integer savedSegmentId;

        @JsonProperty ("prebuild_segment_id")
        @Getter
        @Setter
        private String prebuildSegmentId;

        @JsonProperty("match")
        @Getter
        @Setter
        private String match;
    }

    public static class Recipients {
        @JsonProperty("list_id")
        @Getter
        @Setter
        private String listId;

        @JsonProperty("list_is_active")
        @Getter
        @Setter
        private Boolean listIsActive;

        @JsonProperty("list_name")
        @Getter
        @Setter
        private String listName;

        @JsonProperty("segment_text")
        @Getter
        @Setter
        private String segmentText;

        @JsonProperty("recipient_count")
        @Getter
        @Setter
        private Integer recipientCount;

        @JsonProperty("segment_opts")
        @Getter
        @Setter
        private SegmentOpts segmentOpts;
    }

    public static class Settings {
        @JsonProperty("subject_line")
        @Getter
        @Setter
        private String subjectLine;

        @JsonProperty("preview_text")
        @Getter
        @Setter
        private String previewText;

        @JsonProperty
        @Getter
        @Setter
        private String title;

        @JsonProperty("from_name")
        @Getter
        @Setter
        private String fromName;

        @JsonProperty("reply_to")
        @Getter
        @Setter
        private String replyTo;

        @JsonProperty("to_name")
        @Getter
        @Setter
        private String toName;

        @JsonProperty("folder_id")
        @Getter
        @Setter
        private String folderId;

        @JsonProperty("template_id")
        @Getter
        @Setter
        private Integer templateId;
    }

    public static class Tracking {
        @JsonProperty("opens")
        @Getter
        @Setter
        private Boolean opens;

        @JsonProperty("html_clicks")
        @Getter
        @Setter
        private Boolean htmlClicks;

        @JsonProperty("text_clicks")
        @Getter
        @Setter
        private Boolean textClicks;

        @JsonProperty("goal_tracking")
        @Getter
        @Setter
        private Boolean goalTracking;

        @JsonProperty("ecomm360")
        @Getter
        @Setter
        private Boolean ecomm;

        @JsonProperty("google_analytics")
        @Getter
        @Setter
        private String googleAnalytics;

        @JsonProperty("clicktale")
        @Getter
        @Setter
        private String clicktale;
    }

    @JsonProperty
    @Getter
    @Setter
    private String id;

    @JsonProperty("web_id")
    @Getter
    @Setter
    private Integer webId;

    @JsonProperty("parent_campaign_id")
    @Getter
    @Setter
    private String parentCampaignId;

    @JsonProperty
    @Getter
    @Setter
    private String type;

    @JsonProperty("create_time")
    @Getter
    @Setter
    private String createTime;

    @JsonProperty("archive_url")
    @Getter
    @Setter
    private String archiveUrl;

    @JsonProperty("long_archive_url")
    @Getter
    @Setter
    private String longArchiveUrl;

    @JsonProperty
    @Getter
    @Setter
    private String status;

    @JsonProperty("emails_sent")
    @Getter
    @Setter
    private Integer emailsSent;

    @JsonProperty("send_time")
    @Getter
    @Setter
    private String sendTime;

    @JsonProperty("content_type")
    @Getter
    @Setter
    private String contentType;

    @JsonProperty("needs_block_refresh")
    @Getter
    @Setter
    private Boolean needsBlockRefresh;

    @JsonProperty("has_logo_merge_tag")
    @Getter
    @Setter
    private Boolean hasLogoMergeTag;

    @JsonProperty
    @Getter
    @Setter
    private Boolean resendable;

    @JsonProperty
    @Getter
    @Setter
    private Recipients recipients;

    @JsonProperty
    @Getter
    @Setter
    private Settings settings;

    @JsonProperty
    @Getter
    @Setter
    private Tracking tracking;
}
